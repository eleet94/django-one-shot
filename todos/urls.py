from django.urls import include, path
from todos.views import todo_list, list_detail


urlpatterns = [
    path("todos/", todo_list, name="todo_list_list"),
    path("todos/detail/<int:id>/", list_detail, name="list_detail"),
    path("__debug__/", include("debug_toolbar.urls")),
    path("__debug__/", include("debug_toolbar.urls")),
]
