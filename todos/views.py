from django.shortcuts import (
    render,
    get_object_or_404,
)
from todos.models import TodoList

# Create your views here.

# class TodoList(models.Model):
#     name = models.CharField(max_length=100)
#     created_on = models.DateTimeField(auto_now_add=True)


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def list_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": details,
    }
    return render(request, "todos/detail.html", context)
